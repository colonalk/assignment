package resources.java.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.WebDriver;

public class RegistrationPage extends BasePage {
	
	WebDriver _driver;
	
	//Constructor that will be automatically called as soon as the object of the class is created
	public RegistrationPage(WebDriver driver) {
		super(driver);
		_driver=driver;
	}
	
	//Locators for personal information
	By MaleCheckBox = By.id("id_gender1");
	By FemaleCheckBox = By.id("id_gender2");
	By FirstNameInput = By.id("customer_firstname");
	By LastNameInput = By.id("customer_lastname");
	By PasswordInput = By.id("passwd");
	By DobDaySelect = By.id("days");
	By DobMonthSelect = By.id("months");
	By DobYearSelect = By.id("years");
	//Locators for your address
	By CompanyInput = By.id("company");
	By AddressInput = By.id("address1");
	By CityInput = By.id("city");
	By StateSelect = By.id("id_state");
	By ZipCodeInput = By.id("postcode");
	By HomePhoneInput = By.id("phone");
	
	By RegisterButton = By.id("submitAccount");
	
	//Method to click login button
	public void InputUserInformation() {
		_driver.findElement(MaleCheckBox).click();
		_driver.findElement(FirstNameInput).sendKeys("test");
		_driver.findElement(LastNameInput).sendKeys("tester");
		_driver.findElement(PasswordInput).sendKeys("tester");
		
		//Select Day
		Select daySelector = new Select(_driver.findElement(DobDaySelect));
		daySelector.selectByIndex(4);
		//Select Day
		Select monthSelector = new Select(_driver.findElement(DobMonthSelect));
		monthSelector.selectByIndex(4);
		//Select Day
		Select yearSelector = new Select(_driver.findElement(DobYearSelect));
		yearSelector.selectByIndex(4);
		
		_driver.findElement(CompanyInput).sendKeys("test");
		_driver.findElement(AddressInput).sendKeys("1 Test Place");
		_driver.findElement(CityInput).sendKeys("Test");
		
		Select stateSelector = new Select(_driver.findElement(StateSelect));
		stateSelector.selectByIndex(4);
		
		_driver.findElement(ZipCodeInput).sendKeys("12345");
		_driver.findElement(HomePhoneInput).sendKeys("0831234567");
    }
	
	public void ClickRegisterButton() {
		_driver.findElement(RegisterButton).click();
    }

}

