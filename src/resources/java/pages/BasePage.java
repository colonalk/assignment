package resources.java.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class BasePage {
	WebDriver _driver;
	protected BasePage(WebDriver driver){
		_driver = driver;
	}
	
	public void ClickUsingJavascript(By selector) {
		WebElement element = _driver.findElement(selector);
		((JavascriptExecutor) _driver).executeScript("arguments[0].click();", element);
	}
	
	public void ScrollToElementUsingJavascript(By selector) {
		WebElement element = _driver.findElement(selector);
		((JavascriptExecutor) _driver).executeScript("arguments[0].scrollIntoView(true);", element);
	}
}
