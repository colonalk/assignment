package resources.java.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class MyAccountPage extends BasePage {
	
	WebDriver _driver;
	
	//Constructor that will be automatically called as soon as the object of the class is created
	public MyAccountPage(WebDriver driver) {
		super(driver);
		_driver=driver;
	}
	
	//Locator for login button
	By BackButton = By.xpath("//*/a[contains(@title, 'Return')]");
	
	//Method to click login button
	public void clickReturnToHome() {
		_driver.findElement(BackButton).click();
	}

}

