package resources.java.utilities;

import java.util.Map;
import io.restassured.RestAssured;
import io.restassured.http.Header;
import io.restassured.http.Headers;
import io.restassured.response.Response;

public class HttpHelper {
	
	String cookie;
	
	public Headers ReturnLoginPostHeaders(String referer) {
		Header requestHeader2 = new Header("Cookie", GetSiteCookie());
		Header requestHeader3 = new Header("Upgrade-Insecure-Requests", "1");
		Header requestHeader4 = new Header("Cache-Control", "max-age=0");
		Header requestHeader5 = new Header("Origin", "http://automationpractice.com");
		Header requestHeader6 = new Header("Host", "automationpractice.com");
		Header requestHeader7 = new Header("Accept-Language", "en-US,en;q=0.9");
		Header requestHeader8 = new Header("Content-Type", "application/x-www-form-urlencoded");
		Header requestHeader9 = new Header("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9");
		Header requestHeader10 = new Header("Accept-Encoding", "gzip, deflate");
		Header requestHeader11 = new Header("Referer", referer);
		Header requestHeader12 = new Header("Connection", "keep-alive");
		
		return new Headers(requestHeader2, requestHeader3, requestHeader4, requestHeader5, requestHeader6, requestHeader7, requestHeader8, requestHeader9, requestHeader10, requestHeader11, requestHeader12);
	}
	
	public Headers ReturnLoginGetHeaders(String referer) {
		Header requestHeader2 = new Header("Cookie", GetSiteCookie());
		Header requestHeader3 = new Header("Upgrade-Insecure-Requests", "1");
		Header requestHeader6 = new Header("Host", "automationpractice.com");
		Header requestHeader7 = new Header("Accept-Language", "en-US,en;q=0.9");
		Header requestHeader9 = new Header("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9");
		Header requestHeader10 = new Header("Accept-Encoding", "gzip, deflate");
		Header requestHeader11 = new Header("Referer", referer);
		Header requestHeader12 = new Header("Connection", "keep-alive");
		
		return new Headers(requestHeader2, requestHeader3, requestHeader6, requestHeader7, requestHeader9, requestHeader10, requestHeader11, requestHeader12);
	}
	
	public String GetSiteCookie() {
		if (cookie == null)
		{
			Response response = RestAssured.get("http://automationpractice.com/index.php");
			
			Map<String,String> cookieData = response.getCookies();
			
			StringBuilder mapAsString = new StringBuilder();
		    for (String key : cookieData.keySet()) {
		        mapAsString.append(key);
		    }
		    mapAsString.append("=");
		    for (String value : cookieData.values()) {
		        mapAsString.append(value);
		    }
			return mapAsString.toString();
		}
		else
		{
			return cookie;
		}

	}
	
	public String ReturnRegistrationBody()
	{
		return "id_gender=1&customer_firstname=test&customer_lastname=tester&email=tewst%40hgsgsregersrgzsgr.com&passwd=tester&days=9&months=9&years=2009&firstname=test&lastname=tester&company=Test&address1=1+Test+Place&address2=&city=Test&id_state=14&postcode=12345&id_country=21&other=&phone=0837934895&phone_mobile=&alias=My+address&dni=&email_create=1&is_new_customer=1&back=my-account&submitAccount=";
	}

}
